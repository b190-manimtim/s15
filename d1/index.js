  // alert("Hello World");
  console.log("Hello World");
  console.
  log
      (
          "Hello again"
      );

  //   Variables
  //   it used to store data
  // Any information that is used by an application is stored in what we called "memory"
  //   When we create variables, certain portions of a device's memory is given "name" that we call "variables"

  //   if the variable without declaring the variable itself, the console would render that the variable that we are calling is "not defined"


  //   Declaring variables
  // variables are initialized with use of let/const keyword
  //   after declaring the variable (declaring a variable means that we have created a variable and it is ready to receive data), failure to assign a value to it would mean that the variable is "undefined" (the variable is existing but has no value)

  //   let myVariable = "hello";
  let myVariable = "Hello";


  console.log(myVariable);

  // Declaring and initializing variables
  let productName = "Desktop Computer";
  console.log(productName)

  let productPrice = 18999;
  console.log(productPrice);

  //   In the context of certain applications, some variables/information are constant and should not be changed.
  //   One exaple in real-world scenario is the interest for loan, savings account, or mortgage interest must not be changed due to implications in computation

  const interest = 3.539;
  console.log(interest);

  //   Reassigning of variable values
  // reassigning a variable value means that we are going to change the initial or previous value into another value

  productName = "Laptop";
  console.log(productName);

  let friend = "Saitama";
  console.log(friend);

  friend = "Genos"
  console.log(friend)


  //   

  //   const variable values cannot and should not be changed by the devs
  //   if we declare a variable using const, can neither update nor the variable value cannot be reassigned

  //   interest = 4.489;
  //   console.log(interest);

  //   When to use Javascript const?
  // as a general rule, always declare a variable with const unless you know that the value will change

  //   Reassigning vs initializing

  let supplier;
  supplier = "John Smith Tradings"
  console.log(supplier);

  a = 5;
  console.log(a);
  var a;

  // scope of variables

  let outerVariable = "Hello"; {
      let innerVariable = "hello again";
      console.log(innerVariable);
  }

  console.log(outerVariable);
  //   console.log(innerVariable);


  //   multiple variable declarations
  let productCode = "CD017",
      productBrand = "Dell";
  console.log(productCode);
  console.log(productBrand);

  let country = "Philippines";
  console.log(country);

  //   concatenating strings;
  let province = "Metro Manila";
  console.log(province + "," + country)

  //   escape character
  let mailAddress = "Metro Manila\n\nPhilippines"
  console.log(mailAddress)

  //   using double quotes and singl quotes for string data types are actually valid in js
  //   if the string has a single quote/apostrophe inside, it is better to use doubles for string indicator so that we won't have to use the escape character
  console.log("John's employees went home early");
  console.log('John\'s employees went home early');

  //   Numbers
  //   integer/whole number

  let headcount = 26;
  console.log(headcount);

  //   decimal/fractions
  let grade = 98.7;
  console.log(grade);

  //   exponential notation
  let planetDistance = 2e10;
  console.log(planetDistance)

  //   combining strings and numbers

  console.log("John's Grad last quarter is " + grade)

  // boolean

  let isMarried = false;
  let inGoodConduct = true;

  console.log("isMarried: " + isMarried);
  console.log("inGoodConduct: " + inGoodConduct);

  //   array
  // arrays are special kinds of data type that are used to store multiple values. It can store different data types but it is normally used to store similar data types.

  //   similat data types stored in array

  let grades = [98.7, 92.1, 90.2, 64.6];
  console.log(grades);

  //   different data type stored in array
  //   It is not avdisable to use different types in an array since it would be confusing for other devs when they read our codes

  let person = ["John", "Smith", 32, true];
  console.log(person);

  //   object data type
  // objects are another special kind of data type that's used to mimic real world objects. They are used to create complex data that contains pieces of information that are relevant to each other.
  let personDetails = {
      fullName: "Juan Dela Cruz",
      age: 35,
      isMarried: true,
      contact: ["09123456789", "09987654321"],
      address: {
          houseNumber: "345",
          city: "Manila"
      }
  };
  console.log(personDetails)


  //   typeof keyword - used if the dev are not sure or wants to assure of what the data type of the variable is
  console.log(typeof personDetails);


  const anime = ["Naruto", "One Piece", "Slamdunk"];
  console.log(anime);

  anime[0] = ["Akame ga kill"];
  console.log(anime);

  //   null data type
  let number = 0;
  let string = "";
  console.log(number);
  console.log(string);

  //   null is used to intentionally express the absence of a value inside a variable in  a declaration/initialization
  //   one clear difference of null vs undefined is that null means that the variable was created and assigned a value that does not hold any value/amount, compared to the undefined wich is concerened with creating a varaible but was not given any value

  let jowa = null;
  console.log(jowa);